package info.hccis.camper.data.springdatajpa;

import info.hccis.camper.entity.Camper;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CamperRepository extends CrudRepository<Camper, Integer> {

    //Can add specific methods such as these
    
    List<Camper> findByLastName(String lastName);
    List<Camper> findByDob(String dob);
    List<Camper> findByFirstName(String firstName);
    List<Camper> findByFirstNameAndLastName(String firstName, String lastName);

}
