package info.hccis.camper.dao;

import info.hccis.camper.entity.Camper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * This class will contain db functionality for working with Campers
 *
 * @author bjmaclean
 * @since 20160929
 */
public class CamperDAO {

    private final static Logger LOGGER = Logger.getLogger(CamperDAO.class.getName());

    public static Camper select(int idIn) {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        Camper theCamper = null;
        try {
            conn = ConnectionUtils.getConnection();
            System.out.println("Loading for: " + idIn);
            sql = "SELECT * FROM camper where id = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, idIn);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                int id = rs.getInt(1);
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String dob = rs.getString("dob");
                theCamper = new Camper(id, firstName, lastName);
                theCamper.setDob(dob);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return theCamper;
    }
}
